import { Component, OnInit, OnDestroy } from '@angular/core';
import { VideoFeedService } from '../core/services/video-feed.service';
import { Video } from '../shared/models/video.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  videos: Video[];
  subscription: Subscription;

  constructor(public videoFeedService: VideoFeedService) { }

  ngOnInit() {
    this.getVideoData();
  }

  getVideoData() {
    this.subscription = this.videoFeedService.getVideos().subscribe(videos => {
      this.videos = videos.items;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
