import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'shortNumber'
})
export class ShortNumberPipe implements PipeTransform {
  shortNumber: string;

  constructor(private decimalPipe: DecimalPipe) { }

  transform(value: number, digits?: any): any {

    switch (true) {
      case (value <= 999) :
        this.shortNumber = value.toString();
        break;
      case (value >= 1000 && value <= 999999) :
        this.shortNumber = this.decimalPipe.transform(value / 1000, digits) + 'K';
        break;
      case (value >= 1000000 && value <= 999999999) :
        this.shortNumber = this.decimalPipe.transform(value / 1000000, digits) + 'M';
        break;
      case (value >= 1000000000  && value <= 999999999999) :
        this.shortNumber = this.decimalPipe.transform(value / 1000000000, digits) + 'B';
        break;
    }

    return this.shortNumber;
  }
}