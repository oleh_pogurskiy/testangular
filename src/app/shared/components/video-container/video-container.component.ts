import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../../models/video.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-container',
  templateUrl: './video-container.component.html',
  styleUrls: ['./video-container.component.scss']
})
export class VideoContainerComponent {
  @Input() video: Video;

  public source = 'youtube';

  constructor(private sanitizer: DomSanitizer) {}

  getUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url ? url : '');
  }

}
