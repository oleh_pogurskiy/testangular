import { NgModule } from '@angular/core';
import { VideoFeedService } from './services/video-feed.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [HttpClientModule],
    providers: [VideoFeedService],
})
export class CoreModule { }
