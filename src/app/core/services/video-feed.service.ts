import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// declare function require(url: string);

// const videoData = require('./../mocks/data.json');

@Injectable()
export class VideoFeedService {

  constructor(private http: HttpClient) {}

  getVideos(): Observable<any> {
    // return of(videoData);
    return this.http.get('http://localhost:3000/videos').pipe(
      map(
        results => results
      ),
      catchError(
        err => {
          console.log('Error', err.message);
          return throwError(err);
        }
      )
    );
  }
}
