import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './core/header/header.component';
import { VideoContainerComponent } from './shared/components/video-container/video-container.component';
import { CoreModule } from './core/core.module';
import { ShortNumberPipe } from './shared/pipes/short-number.pipe';
import { DecimalPipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    VideoContainerComponent,
    ShortNumberPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [
    ShortNumberPipe,
    DecimalPipe
  ],
  exports: [
    ShortNumberPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
